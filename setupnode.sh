apt-get update && apt install -y software-properties-common
apt-add-repository -y ppa:bitcoin/bitcoin
apt update
apt-get install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" make software-properties-common \
build-essential libtool autoconf libssl-dev libboost-dev libboost-chrono-dev libboost-filesystem-dev libboost-program-options-dev \
libboost-system-dev libboost-test-dev libboost-thread-dev sudo automake git wget curl libdb4.8-dev bsdmainutils libdb4.8++-dev \
libminiupnpc-dev libgmp3-dev ufw pkg-config libevent-dev  libdb5.3++ unzip libzmq5

git clone https://gitlab.com/akshaycm/hivenetwork
cd hivenetwork
bash autogen.sh
chmod -R 775 *
./configure --enable-tests=no --with-gui=no
make install
