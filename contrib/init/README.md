Sample configuration files for:

SystemD: hived.service
Upstart: hived.conf
OpenRC:  hived.openrc
         hived.openrcconf
CentOS:  hived.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
