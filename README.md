HIVE (HIVE) is a universal 3rd generation cryptocurrency that combines the best features of different coins
in order to create an excellent new digital payment asset.

Milli-Second-Transactions with HyperSend.
Network stability with masternodes, each is secured with a collateral of 10.000 HIVE. 100% Governance.

For further information visit us at [hive.org](https://hive.org/) or visit our ANN thread at [BitcoinTalk](https://bitcointalk.org/index.php?topic=2343884.0)

# Coin Specs

• Coin Name: HIVE Network 
• Ticker: HIVE  
• PoW Algorithm: Quark  
• Premine: (#1 Block) 100,001 HIVE (0,17% of PoW)*  
• PoW Blocks: 2 - 475200  
• PoS Blocks: Starting from 475201  
• Block Time: 60 Seconds  
• PoW Max Coin Output/Supply: 57,879,300  
• PoW Ending: ~ ca. 330 Days (Estimated: October 2018)  
• Masternode Requirements: 10,000 HIVE  
• Maturity: 111 Confirmations  
• Prefix: HIVE adresses start with the capital letter "A"   

*the premine is going to be burned at the 1st November 2018

# PoW Reward Distribution

<table>
  <tr><th>Block Height</th><th>Reward Amount</th><th>Masternodes</th><th>Miners</th><th>DevFee</th></tr>
  <tr><td>Block 2 - 86400</td><td>200 HIVE</td><td>   Masternodes 40 HIVE</td><td>Miners 160 HIVE</td><td>0 HIVE</td></tr>
<tr><td>Block 86401 - 151200</td><td>150 HIVE</td><td>  Masternodes 37,5 HIVE</td><td>Miners 112,5 HIVE</td><td>0 HIVE</td></tr>
<tr><td>Block 151201 - 225000</td><td>125 HIVE</td><td>  Masternodes 37,5 HIVE</td><td>Miners 87,5 HIVE</td><td>0 HIVE</td></tr>
<tr><td>Block 225001 - 302400</td><td>125 HIVE</td><td>  Masternodes 75 HIVE</td><td>Miners 43,75 HIVE</td><td>6,25 HIVE</td></tr>
<tr><td>Block 302401 - 345600</td><td>100 HIVE</td><td>   Masternodes 60 HIVE</td><td>Miners 35 HIVE</td><td>5 HIVE</td></tr>
<tr><td>Block 345601 - 388800</td><td>75 HIVE</td><td>      Masternodes 45 HIVE</td><td>Miners 26,25 HIVE</td><td>3,75 HIVE</td></tr>
<tr><td>Block 388801 - 475200</td><td>50 HIVE</td><td>      Masternodes 30 HIVE</td><td>Miners 17,5 HIVE</td><td>2,5 HIVE</td></tr>
</table>

Masternode and staker rewards with PoS:
Masternodes and stakers will receive split reward allocation when PoS sets in.
This means the more masternodes are in the system, the more staking gets attractive and vice versa.

# PoS Block Rewards

_Proof of Stake will automatically be enabled at block 475201._
<table>
<tr><th>Block Height</th><th>Reward Amount</th>                  
<tr><td>Block 475201 - 518400</td><td>50 HIVE</td></tr>
<tr><td>Block 518401 - 561600</td><td>25 HIVE</td></tr>
<tr><td>Block 561601 - 604800</td><td>10 HIVE</td></tr>
<tr><td>Block 604801 - infinite</td><td>5 HIVE</td></tr>
</table>


# Further information

_For more information check out our whitepaper at [hive.org/whitepaper.pdf](https://hive.org/whitepaper.pdf)_


Alternatively we have added a non .pdf Version into our Github. You can find it in the HIVE Github Wiki.

[https://github.com/HIVECRYPTO/HIVE/wiki](https://github.com/HIVECRYPTO/HIVE/wiki)
